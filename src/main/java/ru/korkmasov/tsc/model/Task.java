package ru.korkmasov.tsc.model;

import ru.korkmasov.tsc.api.entity.ITWBS;
import ru.korkmasov.tsc.enumerated.Status;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class Task extends AbstractOwner implements ITWBS {

    @Nullable
    private String projectId;

    @Nullable
    private String id = UUID.randomUUID().toString();

    @Nullable
    private String name = "";

    @Nullable
    private String description = "";

    @Nullable
    private Status status = Status.NOT_STARTED;

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @Nullable
    private Date created;

    public void setStatus(@Nullable final Status status) {
        if (status == null) return;
        this.status = status;
        switch (status) {
            case IN_PROGRESS:
                this.setDateStart(new Date());
                break;
            case COMPLETE:
                this.setDateFinish(new Date());
            default:
                break;
        }
    }

    public Task(@Nullable String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return id + ": " + name;
    }

}
